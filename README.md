0. Introduktion:
	Bas-tema som jag (Kalle) använder för nya (och kanske) gamla projekt. Det fungerar helt okej att migrera en gammal sajt till det här temat men då får ni ändra lite inställningar (wp-config, htaccess, functions.php mfl.). Syftet är att göra en bra bas med bra struktur som inte är bloated med för mycket som ändå inte kommer användas. Väldigt cleant. Ingen markup, några få js-funktioner men that's it. Mest fokus är funktionalitet, struktur och säkerhet.

1. Installation:
	1.1. Byt namn på wp-config-sample.php > wp-config.php.
	1.2. Gör ändringar som behövs.
	1.3. Byt namn på itc.htaccess > .htaccess.
	1.4. Gör eventuella ändringar. Kom ihåg att lägga till (ifall det är en gammal sajt): RewriteRule ^wp-content/uploads/(.*) /itc-content/uploads/$1 [QSA,L]

2. Övrigt:
	Hör av er vid frågor.
