<?php
	add_theme_support( 'menus' );
	add_theme_support( 'post-thumbnails' );
	date_default_timezone_set('Europe/Stockholm');
	setlocale(LC_ALL, 'sv_SE');

	require_once('assets/extensions/itc.security.php');						// Security Fixes.
	//require_once('assets/extensions/wp_itc_authenticate.php');	// Security Extension - ITC Brute Force Protection.
	require_once('assets/extensions/itc.remove_comments.php');		// Remove Comments.
	require_once('assets/extensions/itc.remove_emojis.php');			// Remove Emojis.
	require_once('assets/extensions/itc.misc.php');								// Misc quality of life changes.
	require_once('assets/extensions/itc.clean_admin.php');				// Cleans up admin.
	//require_once('assets/extensions/aq_resizer.php'); 					// 1.2.1 /w ITC Quality Setting. Default: 80.

	// Add custom /assets rewrite.
	// Example http://www.domain.com/assets/css/style.css
	// Points towards template directory/assets/$i
	function itc_add_rewrites( $wp_rewrite ) {
		$path = str_replace( home_url( '/' ), '', get_template_directory_uri() );
		
		$wp_rewrite->non_wp_rules += array(
			'assets/(.*)' => $path . '/assets/$1',
		);
	}
	add_action( 'generate_rewrite_rules', 'itc_add_rewrites' );
	
	function clean_filename($filename) {
		$transliterationTable = array('á' => 'a', 'Á' => 'A', 'à' => 'a',
		'À' => 'A', 'ă' => 'a', 'Ă' => 'A', 'â' => 'a', 'Â' => 'A',
		'å' => 'a', 'Å' => 'A', 'ã' => 'a', 'Ã' => 'A', 'ą' => 'a',
		'Ą' => 'A', 'ā' => 'a', 'Ā' => 'A', 'ä' => 'ae', 'Ä' => 'AE',
		'æ' => 'ae', 'Æ' => 'AE', 'ḃ' => 'b', 'Ḃ' => 'B', 'ć' => 'c',
		'Ć' => 'C', 'ĉ' => 'c', 'Ĉ' => 'C', 'č' => 'c', 'Č' => 'C',
		'ċ' => 'c', 'Ċ' => 'C', 'ç' => 'c', 'Ç' => 'C', 'ď' => 'd',
		'Ď' => 'D', 'ḋ' => 'd', 'Ḋ' => 'D', 'đ' => 'd', 'Đ' => 'D',
		'ð' => 'dh', 'Ð' => 'Dh', 'é' => 'e', 'É' => 'E', 'è' => 'e',
		'È' => 'E', 'ĕ' => 'e', 'Ĕ' => 'E', 'ê' => 'e', 'Ê' => 'E',
		'ě' => 'e', 'Ě' => 'E', 'ë' => 'e', 'Ë' => 'E', 'ė' => 'e',
		'Ė' => 'E', 'ę' => 'e', 'Ę' => 'E', 'ē' => 'e', 'Ē' => 'E',
		'ḟ' => 'f', 'Ḟ' => 'F', 'ƒ' => 'f', 'Ƒ' => 'F', 'ğ' => 'g',
		'Ğ' => 'G', 'ĝ' => 'g', 'Ĝ' => 'G', 'ġ' => 'g', 'Ġ' => 'G',
		'ģ' => 'g', 'Ģ' => 'G', 'ĥ' => 'h', 'Ĥ' => 'H', 'ħ' => 'h',
		'Ħ' => 'H', 'í' => 'i', 'Í' => 'I', 'ì' => 'i', 'Ì' => 'I',
		'î' => 'i', 'Î' => 'I', 'ï' => 'i', 'Ï' => 'I', 'ĩ' => 'i',
		'Ĩ' => 'I', 'į' => 'i', 'Į' => 'I', 'ī' => 'i', 'Ī' => 'I',
		'ĵ' => 'j', 'Ĵ' => 'J', 'ķ' => 'k', 'Ķ' => 'K', 'ĺ' => 'l',
		'Ĺ' => 'L', 'ľ' => 'l', 'Ľ' => 'L', 'ļ' => 'l', 'Ļ' => 'L',
		'ł' => 'l', 'Ł' => 'L', 'ṁ' => 'm', 'Ṁ' => 'M', 'ń' => 'n',
		'Ń' => 'N', 'ň' => 'n', 'Ň' => 'N', 'ñ' => 'n', 'Ñ' => 'N',
		'ņ' => 'n', 'Ņ' => 'N', 'ó' => 'o', 'Ó' => 'O', 'ò' => 'o',
		'Ò' => 'O', 'ô' => 'o', 'Ô' => 'O', 'ő' => 'o', 'Ő' => 'O',
		'õ' => 'o', 'Õ' => 'O', 'ø' => 'o', 'Ø' => 'O', 'ō' => 'o',
		'Ō' => 'O', 'ơ' => 'o', 'Ơ' => 'O', 'ö' => 'o', 'Ö' => 'O',
		'ṗ' => 'p', 'Ṗ' => 'P', 'ŕ' => 'r', 'Ŕ' => 'R', 'ř' => 'r',
		'Ř' => 'R', 'ŗ' => 'r', 'Ŗ' => 'R', 'ś' => 's', 'Ś' => 'S',
		'ŝ' => 's', 'Ŝ' => 'S', 'š' => 's', 'Š' => 'S', 'ṡ' => 's',
		'Ṡ' => 'S', 'ş' => 's', 'Ş' => 'S', 'ș' => 's', 'Ș' => 'S',
		'ß' => 'SS', 'ť' => 't', 'Ť' => 'T', 'ṫ' => 't', 'Ṫ' => 'T',
		'ţ' => 't', 'Ţ' => 'T', 'ț' => 't', 'Ț' => 'T', 'ŧ' => 't',
		'Ŧ' => 'T', 'ú' => 'u', 'Ú' => 'U', 'ù' => 'u', 'Ù' => 'U',
		'ŭ' => 'u', 'Ŭ' => 'U', 'û' => 'u', 'Û' => 'U', 'ů' => 'u',
		'Ů' => 'U', 'ű' => 'u', 'Ű' => 'U', 'ũ' => 'u', 'Ũ' => 'U',
		'ų' => 'u', 'Ų' => 'U', 'ū' => 'u', 'Ū' => 'U', 'ư' => 'u',
		'Ư' => 'U', 'ü' => 'ue', 'Ü' => 'UE', 'ẃ' => 'w', 'Ẃ' => 'W',
		'ẁ' => 'w', 'Ẁ' => 'W', 'ŵ' => 'w', 'Ŵ' => 'W', 'ẅ' => 'w',
		'Ẅ' => 'W', 'ý' => 'y', 'Ý' => 'Y', 'ỳ' => 'y', 'Ỳ' => 'Y',
		'ŷ' => 'y', 'Ŷ' => 'Y', 'ÿ' => 'y', 'Ÿ' => 'Y', 'ź' => 'z',
		'Ź' => 'Z', 'ž' => 'z', 'Ž' => 'Z', 'ż' => 'z', 'Ż' => 'Z',
		'þ' => 'th', 'Þ' => 'Th', 'µ' => 'u', 'а' => 'a', 'А' => 'a',
		'б' => 'b', 'Б' => 'b', 'в' => 'v', 'В' => 'v', 'г' => 'g',
		'Г' => 'g', 'д' => 'd', 'Д' => 'd', 'е' => 'e', 'Е' => 'E',
		'ё' => 'e', 'Ё' => 'E', 'ж' => 'zh', 'Ж' => 'zh', 'з' => 'z',
		'З' => 'z', 'и' => 'i', 'И' => 'i', 'й' => 'j', 'Й' => 'j',
		'к' => 'k', 'К' => 'k', 'л' => 'l', 'Л' => 'l', 'м' => 'm',
		'М' => 'm', 'н' => 'n', 'Н' => 'n', 'о' => 'o', 'О' => 'o',
		'п' => 'p', 'П' => 'p', 'р' => 'r', 'Р' => 'r', 'с' => 's',
		'С' => 's', 'т' => 't', 'Т' => 't', 'у' => 'u', 'У' => 'u',
		'ф' => 'f', 'Ф' => 'f', 'х' => 'h', 'Х' => 'h', 'ц' => 'c',
		'Ц' => 'c', 'ч' => 'ch', 'Ч' => 'ch', 'ш' => 'sh', 'Ш' => 'sh',
		'щ' => 'sch', 'Щ' => 'sch', 'ъ' => '', 'Ъ' => '', 'ы' => 'y',
		'Ы' => 'y', 'ь' => '', 'Ь' => '', 'э' => 'e', 'Э' => 'e',
		'ю' => 'ju', 'Ю' => 'ju', 'я' => 'ja', 'Я' => 'ja');

		$filename = str_replace(array_keys($transliterationTable), array_values($transliterationTable), $filename);
		$filename = remove_accents($filename);
		$filename = strtolower($filename);
		$filename = preg_replace("/[^a-z0-9\/_|+ .-]/", "", $filename);
		$filename = preg_replace("/[\s-]+/", " ", $filename);
		$filename = preg_replace("/[\s_]/", "-", $filename);

		return $filename;
	}
	add_filter('sanitize_file_name', 'clean_filename', 10, 2);
	
	function itc_create_custom_post_types() {
		/*register_taxonomy( 'prod',
			array( 'produkt' ), array(
				'show_ui' 					=> true,
				'show_in_nav_menus' => false,
				'rewrite' 					=> array( 
																'rewrite' => false, 
																'hierarchical' => true, 
																'with_front' => false 
				),
				'menu_name' 				=> 'Kategori',
				'labels' 						=> array(
					'name' 						=> __( 'Kategori' ),
					'singular_name' 	=> __( 'Kategori' )
				),
			)
		);

		register_post_type( 'inspiration',
			array(
				'public' 								=> true,
				'publicly_queryable' 		=> true,
				'capability_type' 			=> 'page',
				'has_archive' 					=> true,
				'show_ui' 							=> true,
				'show_in_menu' 					=> true,
				'hierarchical'					=> true,
				'rewrite' 							=> array(
																		'slug' => 
																		'passion-for-sas'
				),
				'labels' => array(
					'name' 								=> __( 'Passion för sås' ),
					'singular_name' 			=> __( 'Passion för sås' ),
					'add_new' 						=> __( 'Lägg till' ),
					'add_new_item' 				=> __( 'Lägg till' ),
					'edit' 								=> __( 'Redigera' ),
					'edit_item' 					=> __( 'Redigera' ),
					'new_item' 						=> __( 'Ny' ),
					'view' 								=> __( 'Visa' ),
					'view_item' 					=> __( 'Visa' ),
					'search_items' 				=> __( 'Sök' ),
					'not_found' 					=> __( ' - ' ),
					'not_found_in_trash' 	=> __( ' - ' ),
					'parent' 							=> __( ' - ' ),
					'menu_name' 					=> 'Passion för sås'
				),
				'supports' => array( 
					'title', 
					'page-attributes', 
					'thumbnail', 
					'editor' 
				)
			)
		);*/
	}
	//add_action( 'init', 'itc_create_custom_post_types' );
