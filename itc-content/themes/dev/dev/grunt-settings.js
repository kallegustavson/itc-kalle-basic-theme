module.exports = {
    sassDir:          '../assets/scss/',
    sassMainFileName: '../assets/scss/style',
    cssDir:           '../assets/css/',
    cssMainFileDir:   '../assets/css/',
    cssMainFileName:  'style',
    jsDir:            '../assets/js/',
    imgDir:           '../assets/images/',
    imgSourceDir:     '../assets/sourceimages/'
  };