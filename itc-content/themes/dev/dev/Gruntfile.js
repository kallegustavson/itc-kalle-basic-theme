module.exports = function(grunt) {

  require('load-grunt-tasks')(grunt);

var PathConfig = require('./grunt-settings.js');

  // tasks
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    config: PathConfig,

    //clean files
    clean: {
      options: { force: true },
      temp: {
        src: ["<%= config.cssDir %>**/*.map", "<%= config.cssMainFileDir %><%= config.cssMainFileName %>.css.map"]
      }
    },
		autoprefixer: {
		 options: {
			 browsers: ['last 4 version', 'Android 4', 'ie 10']
		 },
		 multiple_files: {
			 options: {
					 map: true
			 },
			 expand: true,
			 flatten: true,
			 src: ['<%= config.cssDir %>*.css', '<%= config.cssMainFileDir %><%= config.cssMainFileName %>.css']
		 },
		 dist: {
			 src: ['<%= config.cssDir %>*.css', 
						 '<%= config.cssMainFileDir %><%= config.cssMainFileName %>.css',
						 ]
		 },
		},
    postcss: {
      dev: {
        options: {
          map: true,
          processors: [
            require('autoprefixer')({browsers: ['last 4 version', 'Android 4']}),
						require("css-mqpacker")(),
						require('cssnano')({ colormin: false, discardComments: {removeAll: true} })
          ]
        },
        src: ['<%= config.cssDir %>*.css',
              '<%= config.cssMainFileDir %><%= config.cssMainFileName %>.css',
              ]
      },
      dist: {
        options: {
          map: false,
          processors: [
            require('autoprefixer')({browsers: ['last 4 version', 'Android 4']})
          ]
        },
        src: ['<%= config.cssDir %>*.css',
              '<%= config.cssMainFileDir %><%= config.cssMainFileName %>.css',
              ]
      }
    },

    //sass
    sass: {
      options: PathConfig.hasBower,
      dev: {
        options: {
          sourceMap: true,
          style: 'nested'
        },
        files: [
          {
            expand: true,
            cwd: '<%= config.sassDir %>',
            src: ['**/*.scss', '!<%= config.sassMainFileName %>.scss'],
            dest: '<%= config.cssDir %>',
            ext: '.css'
          },
          {src: '<%= config.sassDir %><%= config.sassMainFileName %>.scss', dest: '<%= config.cssMainFileDir %><%= config.cssMainFileName %>.css'}
        ]
      },
      dist: {
        options: {
          sourceMap: false,
          style: 'nested'
        },
        files: [
          {
            expand: true,
            cwd: '<%= config.sassDir %>',
            src: ['**/*.scss', '!<%= config.sassMainFileName %>.scss'],
            dest: '<%= config.cssDir %>',
            ext: '.css'
          },
          {src: '<%= config.sassDir %><%= config.sassMainFileName %>.scss', dest: '<%= config.cssMainFileDir %><%= config.cssMainFileName %>.css'}
        ]
      }
    },

    //watcher project
    watch: {
      options: {
        debounceDelay: 1,
        // livereload: true,
      },
      css: {
        files: ['<%= config.sassDir %>**/*.scss'],
        tasks: ['sass:dev', 'postcss:dev'],
        options: {
            spawn: false,
        }
      }
    },

    csscomb: {
      all: {
        expand: true,
        src: ['<%= config.cssDir %>*.css', 
              '<%= config.cssMainFileDir %><%= config.cssMainFileName %>.css',
              ],
        ext: '.css'
      },
      dist: {
        expand: true,
        files: {
          '<%= config.cssMainFileDir %><%= config.cssMainFileName %>.css' : '<%= config.cssMainFileDir %><%= config.cssMainFileName %>.css'
        },
      }
    },

  });

	// run task
  grunt.registerTask('w', ['watch']);
  grunt.registerTask('dev', ['watch']);
  grunt.registerTask('default', ['dev']);	
  grunt.registerTask('cssbeauty', ['sass:dist', 'postcss:dist', 'csscomb:dist']);
  grunt.registerTask('dist', ['clean:temp', 'cssbeauty']);
				
	grunt.loadNpmTasks('grunt-contrib-watch');
	

};



