<?php
	function itc_remove_comment_support() {
		remove_post_type_support( 'post', 'comments' );
		remove_post_type_support( 'page', 'comments' );
	}
	add_action('init', 'itc_remove_comment_support', 100);

	function itc_remove_comment_admin_bar() {
		global $wp_admin_bar;
		$wp_admin_bar->remove_menu('comments');
	}
	add_action( 'wp_before_admin_bar_render', 'itc_remove_comment_admin_bar' );

	function itc_remove_admin_menu_comments() {	
		remove_menu_page( 'edit-comments.php' );
	}
	add_action( 'admin_menu', 'itc_remove_admin_menu_comments' );