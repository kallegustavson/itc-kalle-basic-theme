<?php

add_filter('authenticate', 'itc_authenticate_check', 99); 
function itc_authenticate_check($user) 
{
	
	// Varnish Support
	$user_ip = $_SERVER['REMOTE_ADDR'];
	if ( isset($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
		$user_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	}
  
  //brute force protection 
  global $wpdb;

  $valid_countries = array ('SE','NO','FI','DK'); //,'FR','GB','GBR');
  $max_retries = 70; //OBS Den här hooken körs två gånger. Så 10 retries = 5 retries egentligen.;
  
  //autoinstall
  if (!$wpdb->query("SHOW TABLES LIKE '".$wpdb->prefix."login_logs'"))
  { 
    $sql = "CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}login_logs` (
    `id` int(20) NOT NULL AUTO_INCREMENT,
    `ip` varchar(20) DEFAULT NULL,
    `country` varchar(4) DEFAULT NULL,
    `retries` int(10) DEFAULT NULL,
    `create_date` date DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `ip,create_date` (`ip`,`create_date`)
    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
        
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
  }
   
  $r = $wpdb->get_row( $wpdb->prepare("SELECT * FROM ".$wpdb->prefix."login_logs where ip = '%s' and create_date='%s' limit 1", $user_ip, date('Y-m-d')) );
  if (isset($r->id))
  {
    $r->retries += 1;
    $wpdb->query($wpdb->prepare("update ".$wpdb->prefix."login_logs set retries = '%d' where id = '%d' limit 1",$r->retries,$r->id));    
    if (!in_array($r->country,$valid_countries)) die('YOUR ACCESS IS BLOCKED, PLEASE CONTACT ADMIN FOR MORE INFORMATION');
    if ($r->retries>$max_retries) die('AUTHENTICATE RETRY BLOCKER'); //OBS Den här hooken körs två gånger. Så 10 retries = 5 retries egentligen.
  }
  else
  {
    if (!preg_match('#^192\.168#',$user_ip) && !preg_match('#^10\.11#',$user_ip))    
    {
      $ip_info = file_get_contents('http://ip2country.inthecold.se/api/?key=xmkmer32RZz&ip='.$user_ip);
      if (empty($ip_info)) die('IP_LOOKUP_FAILED');
      else {
        $ip_info = json_decode($ip_info,false);
        if (!in_array($ip_info->country,$valid_countries)) die('YOUR ACCESS IS BLOCKED, PLEASE CONTACT ADMIN FOR MORE INFORMATION');
      }

      $wpdb->query($wpdb->prepare( 
        "INSERT INTO ".$wpdb->prefix."login_logs
        ( id, ip, country, retries, create_date )
        VALUES ( %d, %s, %s, %d, %s )", 
        array('',$user_ip,$ip_info->country,1,date('Y-m-d'))
      ));      
    }

  }
  return $user;
}

add_action('wp_login', 'itc_login_accepted');
function itc_login_accepted()
{
	
	// Varnish Support
	$user_ip = $_SERVER['REMOTE_ADDR'];
	if ( isset($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
		$user_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	}
	
  //brute force protection clean up on login
  global $wpdb;     
  $wpdb->query($wpdb->prepare("delete from ".$wpdb->prefix."login_logs where ip = '%s' and create_date = '%s' limit 1",$user_ip,date('Y-m-d')));  
}
?>