<?php

	function itc_admin_logo() {
	echo '<style type="text/css"> 
					body.login div#login h1 a {
						background-image: url(' .get_template_directory_uri(). '/assets/img/itc-logo-symbol.svg);
					} 
	</style>';

	}

	add_action( 'login_enqueue_scripts', 'itc_admin_logo' );

	function itc_remove_admin_login_header() {
		remove_action('wp_head', '_admin_bar_bump_cb');
	}
	add_action('get_header', 'itc_remove_admin_login_header');