<?php

	function itc_remove_wp_functions() {
		// Emoticons
		remove_action( 'admin_print_styles', 'print_emoji_styles' );
		remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
		remove_action( 'wp_print_styles', 'print_emoji_styles' );
		remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
		remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
		remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
		// Emoticons end
		
		// ETC
		remove_action( 'wp_head', 'wp_site_icon' );
		remove_action( 'the_content', 'convert_smilies' );
		remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
		remove_action( 'wp_hea	d', 'wp_oembed_add_discovery_links', 10 );
		// ETC End
		
		// Disable REST API
		remove_action( 'template_redirect', 'rest_output_link_header', 11, 0 );
		add_filter( 'rest_enabled', '_return_false' );
		add_filter( 'rest_jsonp_enabled', '_return_false' );
		// Disable REST API End
		
		// De-register Scripts
		
		// Removes auto-embed for youtube, images, tweets and such
		wp_deregister_script('wp-embed');
		
		// Remove js for comments
		wp_deregister_script( 'comment-reply' );
	}
	add_action( 'init', 'itc_remove_wp_functions' );

	function disable_emojicons_tinymce( $plugins ) {
		if ( is_array( $plugins ) ) {
			return array_diff( $plugins, array( 'wpemoji' ) );
		} else {
			return array();
		}
	}
	add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );