<?php

	// Hide Wordpress Version
	function itc_remove_wpv() {
		return '';
	}
	add_filter('the_generator', 'itc_remove_wpv');

	// Block API Request in WP-Admin.
	function wp_api_block_request($pre, $args, $url) {
		if (strpos($url, 'api.wordpress.org')) {
				return true;
		} else {
				return $pre;
		}
	}
	add_filter('pre_http_request', 'wp_api_block_request', 10, 3);

	// Remove Query Strings from Static Resources.
	function _remove_script_version( $src ) {
		$parts = explode( '?ver', $src );
		return $parts[0];
	}

	add_filter( 'script_loader_src', '_remove_script_version', 15, 1 );
	add_filter( 'style_loader_src', '_remove_script_version', 15, 1 );

	function itc_login_error() {
		return 'Wrong username or password.';
	}
	add_filter( 'login_errors', 'itc_login_error' );