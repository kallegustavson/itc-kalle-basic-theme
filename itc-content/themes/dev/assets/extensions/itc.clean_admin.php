<?php

	function itc_remove_dashboard_widgets() {
		remove_meta_box( 'dashboard_activity', 'dashboard', 'normal' );   // Content Activity
		remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );   // Right Now
		remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' ); // Recent Comments
		remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );  // Incoming Links
		remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );   // Plugins
		remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );  // Quick Press
		remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );  // Recent Drafts
		remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );   // WordPress blog
		remove_meta_box( 'dashboard_secondary', 'dashboard', 'side' );   // Other WordPress News
	}
	add_action( 'wp_dashboard_setup', 'itc_remove_dashboard_widgets' );

	function itc_remove_admin_menu_pages() {	
		global $user_ID;
		global $submenu;
		if ( $user_ID != 1 ) {
			remove_menu_page( 'plugins.php' );
			remove_menu_page( 'options-general.php' );
			remove_menu_page( 'tools.php' );
			add_menu_page( 'Menus', 'Menus','edit_theme_options', 'nav-menus.php','','',10);
			remove_menu_page( 'themes.php' );
			//remove_menu_page('sitepress-multilingual-cms/menu/languages.php'); // WPML
			//remove_menu_page('edit.php?post_type=acf-field-group'); // ACF
		}
	}
	add_action( 'admin_menu', 'itc_remove_admin_menu_pages' );