function elementExsist(element) {
	if($(element).length > 0) {
		return true;
	}
	else {
		return false;
	}
}

function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/);
    return pattern.test(emailAddress);
}