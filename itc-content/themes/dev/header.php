<!doctype html>
	<html lang="sv_SE">
	<head>
		<title><?php wp_title( '|', true, 'right' ); ?> <?php bloginfo('name'); ?></title>
		<meta charset="UTF-8">
		<meta name="format-detection" content="telephone=no">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width">
		<link rel="dns-prefetch" href="//fonts.googleapis.com">
		<link rel="dns-prefetch" href="//ajax.googleapis.com">
		<link rel="dns-prefetch" href="//www.google-analytics.com">
		<link rel="dns-prefetch" href="//connect.facebook.net">
		<link rel="dns-prefetch" href="//static.ak.facebook.com">
		<link rel="dns-prefetch" href="//s-static.ak.facebook.com">
		<link rel="dns-prefetch" href="//fbstatic-a.akamaihd.net">
		<link rel="stylesheet" type="text/css" href="<?php bloginfo( 'template_directory' ); ?>/assets/css/style.min.css" media="all">
		<?php wp_head(); ?>
	</head>
	<?php $class = ''; ?>
	<body <?php body_class( $class ); ?>>
<?php flush(); ?>