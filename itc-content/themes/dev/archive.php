<?php
	get_header();
		if ( have_posts() ) :
			while ( have_posts() ) : the_post();
				echo '<article>';
				echo '<h1>'.get_the_title().'</h1>';
				the_excerpt();
				echo '</article>';
			endwhile;
		endif;
	get_footer();
?>