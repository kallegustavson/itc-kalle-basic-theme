<?php
define('DB_NAME', '');
define('DB_USER', '');
define('DB_PASSWORD', '');
define('DB_HOST', 'localhost');
define('DB_CHARSET', 'utf8');
define('DB_COLLATE', '');
define('AUTH_KEY',         'put your unique phrase here');
define('SECURE_AUTH_KEY',  'put your unique phrase here');
define('LOGGED_IN_KEY',    'put your unique phrase here');
define('NONCE_KEY',        'put your unique phrase here');
define('AUTH_SALT',        'put your unique phrase here');
define('SECURE_AUTH_SALT', 'put your unique phrase here');
define('LOGGED_IN_SALT',   'put your unique phrase here');
define('NONCE_SALT',       'put your unique phrase here');

$table_prefix  = 'wp_itc_';
define('WP_DEBUG', false);
define( 'SCRIPT_DEBUG', false );
define( 'WP_DEBUG_LOG', false ); // The Log-file will be placed in wp-content/log.txt

// Extras START //
// WP_CONTENT_DIR | WP_CONTENT_URL: Change default wp-content folder.
define( 'WP_CONTENT_DIR', dirname( __FILE__ ) . '/itc-content' );
define( 'WP_CONTENT_URL', 'http://' . $_SERVER['HTTP_HOST'] . '/itc-content' );

// WP_PLUGIN_DIR | WP_PLUGIN_URL: Change default plugins folder.
define( 'WP_PLUGIN_DIR', dirname(__FILE__) . '/itc-content/itc-plugins' );
define( 'WP_PLUGIN_URL', 'http://' . $_SERVER['HTTP_HOST'] . '/itc-content/itc-plugins' );

// Updates
define('WP_AUTO_UPDATE_CORE', false);
define('AUTOMATIC_UPDATER_DISABLED', true);

// ETC
define('WP_POST_REVISIONS', false);
define('AUTOSAVE_INTERVAL', 320);
define('CONCATENATE_SCRIPTS', false);
define('EMPTY_TRASH_DAYS', 7);
define('DISALLOW_FILE_MODS', true);
define('DISALLOW_FILE_EDIT', true);
define('DISABLE_WP_CRON', false);
define( 'WP_CRON_LOCK_TIMEOUT', 60 );
define( 'WP_HTTP_BLOCK_EXTERNAL', true );
//define( 'WP_ACCESSIBLE_HOSTS', 'api.wordpress.org,*.github.com' );

// Extras END


if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
require_once(ABSPATH . 'wp-settings.php');
